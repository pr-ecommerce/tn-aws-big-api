# Method 4. Terraform

Terraform is a great Infrastructure-as-Code (IaC) tool, that allow teams describe complex solutions in text files, and deploy to not only AWS but other cloud providers.

Terraform is the tool of choice of many DevOps teams across the globe, given its powerful dry-run and state management capabilities.

You need to have the Terraform binary installed in your system for this example to work, installation details can be found here:
https://learn.hashicorp.com/tutorials/terraform/install-cli

Instructions for this example: 

1. Please take a look at the Terraform template in this directory (name "TerraformTemplate.tf").

    Please note you could provision multiple resources and their relationships with a single template and manage them together, very similar to what can be done with CloudFormation but with multi-cloud and solid dry-run (plan) capabilities.

2. From the CLI, you can create the new instance with the following command (run it from this same directory in your terminal):

```
terraform apply     
```

To delete the resources once done you can run:

```
terraform destroy     
```