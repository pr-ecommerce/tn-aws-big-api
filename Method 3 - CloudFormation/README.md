# Method 3. CloudFormation

Pre-requisites: 

[Install AWS-CLI and Configure Programmatic Credentials](https://pipingrock.atlassian.net/wiki/spaces/DEV/pages/2769485825/AWS+Sandbox+account+Access+and+Setup#AWS-Command-line-or-programmatic-access-Setup).

1. Take a look at the CloudFormation template in this directory (name "CloudFormationTemplate.yaml").

    Please note you could provision multiple resources and their relationships with a single template and manage them together, this is the main goal and power of CloudFormation.

2. From the CLI, you can create the new instance with the following command (run it from this same directory in your terminal):

```
aws cloudformation deploy --template-file CloudFormationTemplate.yaml --stack-name EC2Example      
```

To destroy the Stack after you are done, you can run:

```
aws cloudformation delete-stack --stack-name EC2Example      
```