# Method 1. AWS Management Console (GUI)

This method is the simplest and most straightforward, using the AWS management interface to create the desired resource(s).

This works well, specially for immediate needs, POCs, simple setups or resources you might not need to replicate or touch much in the future.

Next to this file, you should be able to find a series of screenshots of how this process looks like. The recommendation is to follow along, and repeat the process in your own AWS Sandbox account.