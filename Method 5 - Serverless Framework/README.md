# Method 4. Serverless Framework

The Serverless Framework is the first framework developed for building Serverless applications on AWS Lambda.

The Serverless Framework has its own YAML configuration, which is compiled and translated into a CloudFormation template for final execution.

Because it is dependent and directly compatible with CloudFormation, any type of AWS resource can also be created through it by embedding CloudFormation syntax along the Serverless Framework code.

Even when it can create every type of resource, it is not the best option for critical or core infrastructure, but for the Serverless application itself. 

Most teams create Databases and other core infrastructure with Terraform (by DevOps), and Serverless Framework is used by developers to build the Serverless applications, referencing outputs and values created by Terraform.

Instructions for this example: 

1. Please take a look at the files "serverless.yml" and "handler.hs".

    Please note you could provision multiple resources and their relationships with a single template and manage them together, very similar to what can be done with CloudFormation but with multi-cloud and solid dry-run (plan) capabilities.

2. From the CLI, you can create the new instance with the following command (run it from this same directory in your terminal):

```
serverless deploy      
```

To delete the resources once done you can run:

```
serverless remove    
```