# AWS, The a Big API

Each sub-directory explores a different method of interaction with the AWS API to create an EC2 instance (Virtual Server).

You should be able to see the instances being provisioned in the AWS Management Console > EC2 > Instances.

The methods explored are:

1. AWS Management Console (GUI)
2. AWS CLI (Terminal)
3. CloudFormation
4. Terraform
5. Serverless Framework

Pre-requisites: 

[Install AWS-CLI and Configure Programmatic Credentials](https://pipingrock.atlassian.net/wiki/spaces/DEV/pages/2769485825/AWS+Sandbox+account+Access+and+Setup#AWS-Command-line-or-programmatic-access-Setup).