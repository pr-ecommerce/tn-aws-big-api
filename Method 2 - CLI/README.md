# Method 2. AWS CLI (Terminal)

Pre-requisites: 

[Install AWS-CLI and Configure Programmatic Credentials](https://pipingrock.atlassian.net/wiki/spaces/DEV/pages/2769485825/AWS+Sandbox+account+Access+and+Setup#AWS-Command-line-or-programmatic-access-Setup).

1. To test your local CLI configuration, the following commands can be executed:

   - aws ec2 describe-instances
   - aws iam get-user

    If you see a JSON output and not an error, you are good ;)

2. Create the EC2 instance with the following commands:

```
# Create a Key Pair to access your instance
aws ec2 create-key-pair --key-name CreatedFromCLIKeyPair

# Create the instance
aws ec2 run-instances --image-id ami-0742b4e673072066f --count 1 \
--instance-type t2.micro --key-name CreatedFromCLIKeyPair \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=CreatedFromCLI}]' 'ResourceType=volume,Tags=[{Key=Name,Value=CreatedFromCLI}]'      
```